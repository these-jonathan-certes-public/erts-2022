
# location where spot is installed:
SPOT="/usr/local/lib/python3.7/site-packages/"

#===============================================================================

all : proof.log
	grep tautology $<


##
# 3) running the proof:
#
proof.log : proof.py
	chmod +x ./$^
	./$^ > $@


##
# 2) translation from SMV to Spot and creation of an implication of the overall
# specifications by the conjunction of the filtered left side properties:
#
proof.py : left_side.spec.smv ${SPEC}
	../../scripts/psl2spot.py  \
	  --properties $<          \
	  --specifications ${SPEC} \
	  --filter ${FILTER}       \
	  --to $@
	# location where spot is installed:
	sed -i '2a import sys' $@
	sed -i '3a sys.path.append(${SPOT})' $@
	sed -i '4G' $@


##
# 1) on the left side of the implication, we have the axioms and the properties
# verified through model-checking:
#
left_side.spec.smv : ${AXIOMS} ${PROP}
	cat $^ > $@

#===============================================================================

clean :
	$(RM) left_side.spec.smv proof.py proof.log
