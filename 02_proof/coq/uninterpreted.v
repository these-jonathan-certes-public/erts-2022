
Require Import LTL.

Section uninterpreted_functions.

  Variable ф₁ ф₂ ф₃ : PathF.
  Variable abstract_ф₂ abstract_ф₃ : PathF.

  Hypothesis model_checked_property:
    ∀ π, π ⊨ ф₁ ∧ ф₂ -> π ⊨ ф₃.

  Hypothesis uninterpreted_phi2:
    ∀ π, π ⊨ abstract_ф₂ <-> π ⊨ ф₂.

  Hypothesis uninterpreted_phi3:
    ∀ π, π ⊨ abstract_ф₃ <-> π ⊨ ф₃.

  Theorem implication:
    ∀ π, π ⊨ ф₁ ∧ abstract_ф₂ -> π ⊨ abstract_ф₃.
  Proof.
    intros π.
    intros H.
    destruct H as (H1 & H2).
    apply uninterpreted_phi2 in H2.
    apply uninterpreted_phi3.
    apply model_checked_property.
    split.
    - exact H1.
    - exact H2.
  Qed.

End uninterpreted_functions.
