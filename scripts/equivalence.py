#!/usr/bin/python3

#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import pynusmv
import sys
import os

##
# \brief
#   Stores the path to the concrete model file after \ref
#   abstraction_parseArgv() has been called.
#
abstraction_concreteModelFilePath = str()

##
# \brief
#   Stores a list of path to the abstracted model files after \ref
#   abstraction_parseArgv() has been called.
#
abstraction_abstractModelFilePathList = list()

##
# \brief
#   Tells if verbose mode is active.
#
abstraction_verboseMode = False

#===============================================================================

##
# \brief
#   Prints help to the standard output and then exits.
#
def abstraction_printHelpAndExit(
  theCode #!< the exit code
):
  theText = """
Verifies the equivalence between transition functions in abstracted and concrete
models

SYNOPSIS:
  ./equivalence.py --concrete FILE1 --abstract FILE2 [FILE]...

DESCRIPTION:
  Uses pynusmv tool to parse model files and compare transition functions. It
  makes sure that all transisions functions in all abstracted models are
  equivalent to some transisions functions in the concrete model.

  In case a transition function in an abstracted model differs from the concrete
  model, both are printed in the standard output.

OPTIONS:
  -c OR --concrete : the argument following this option gives a path to the file
                     containing the description of the concrete model.

  -a OR --abstract : all arguments following this option are considered as a
                     path to the file containing the description of the
                     abstracted model.

  -v OR --verbose  : activates verbose mode. Any mismatch between transition
                     functions causes a print of their definitions in both
                     models.

  -h OR --help     : prints this help and exits

EXAMPLE:
  ./equivalence.py --concrete my_model.smv \\
                   --abstract model1.smv model2.smv model3.smv
"""
  print(str.strip(theText))
  exit(int(theCode))

#===============================================================================

##
# \brief
#   Creates a list of NuSMV node pointers that all share the same type
#   attribute.
#
# \details
#   Recursively accesses NuSMV nodes from one specific node and compares their
#   type: if it matches the expected type, adds them to the list that is given
#   as a parameter. Stops recursively accessing nodes once the type cannot have
#   a hierarchy.
#
# \returns void
#
def recursivelyGetAllNodes(
  theTopNode,   #!< the node to get in the hierarchy from
  theNodeType,  #!< the type of nodes to add to the list
  theList       #!< the list to append
):
  if ( theTopNode.type == theNodeType ):
    list.append(theList, theTopNode)
  else:
    if theTopNode.type not in {
      pynusmv.nusmv.parser.parser.NUMBER_SIGNED_WORD,
      pynusmv.nusmv.parser.parser.NUMBER_UNSIGNED_WORD,
      pynusmv.nusmv.parser.parser.NUMBER_FRAC,
      pynusmv.nusmv.parser.parser.NUMBER_EXP,
      pynusmv.nusmv.parser.parser.NUMBER_REAL,
      pynusmv.nusmv.parser.parser.NUMBER,
      pynusmv.nusmv.parser.parser.ATOM,
      pynusmv.nusmv.parser.parser.CONS,
      pynusmv.nusmv.parser.parser.FAILURE
    }:
      if ( pynusmv.nusmv.node.node.car(theTopNode) is not None ):
        recursivelyGetAllNodes( pynusmv.nusmv.node.node.car(theTopNode),
                                theNodeType,
                                theList
                              )
      if ( pynusmv.nusmv.node.node.cdr(theTopNode) is not None ):
        recursivelyGetAllNodes( pynusmv.nusmv.node.node.cdr(theTopNode),
                                theNodeType,
                                theList
                              )

#===============================================================================

##
# \brief
#   Reads a SMV model file and accesses the transition functions while replacing
#   all <tt> DEFINE </tt> expressions by their values.
#
# \details
#   Uses \ref recursivelyGetAllNodes for both <tt> DEFINE </tt> equalities and
#   <tt> TRANS </tt> nodes. Creates a dictionnary out of <tt> DEFINE </tt>
#   equalities and uses it to replace expressions by their values in the string
#   describing the transition function.
#
# \returns
#   the list of TRANS strings.
#
def abstraction_getTransList(
  theFilePath #!< path to the SMV file to parse
):
  theTransList = list()

  pynusmv.nusmv.parser.parser.Parser_ReadSMVFromFile(theFilePath)
  theTopNode = pynusmv.nusmv.parser.parser.cvar.parsed_tree

  # gets all DEFINE equality nodes:
  theEqdefNodeList = list()
  recursivelyGetAllNodes( theTopNode,
                          pynusmv.nusmv.parser.parser.EQDEF,
                          theEqdefNodeList
                        )

  # creates a dictionnary out of the node values:
  theDefineDict = dict()
  for theEqdefNode in theEqdefNodeList:
    theLeftNode  = pynusmv.nusmv.node.node.car(theEqdefNode)
    theRightNode = pynusmv.nusmv.node.node.cdr(theEqdefNode)
    #
    theDefineName = pynusmv.nusmv.node.node.sprint_node(theLeftNode)
    theDefineName = str.strip(theDefineName)
    #
    theDefineValue = pynusmv.nusmv.node.node.sprint_node(theRightNode)
    theDefineValue = str.strip(theDefineValue)
    #
    theDefineDict[theDefineName] = theDefineValue

  # gets all TRANS nodes:
  theTransNodeList = list()
  recursivelyGetAllNodes( theTopNode,
                          pynusmv.nusmv.parser.parser.TRANS,
                          theTransNodeList
                        )

  # replace DEFINE expressions by their values:
  for theTransNode in theTransNodeList:
    theLeftNode = pynusmv.nusmv.node.node.car(theTransNode)
    theString   = pynusmv.nusmv.node.node.sprint_node(theLeftNode)
    #
    doReplace = True
    while ( doReplace ):
      doReplace = False
      for theKey in theDefineDict:
        if ( theKey in theString ):
          doReplace = True
          theString = str.replace( theString,
                                   theKey,
                                   "(" + theDefineDict[theKey] + ")"
                                 )
    #
    list.append(theTransList, theString)

  return theTransList

#===============================================================================

##
# \brief
#   Parses the vector of arguments and updates global variables \ref
#   abstraction_concreteModelFilePath and \ref
#   abstraction_abstractModelFilePathList.
#
# \details
#   More informations about the syntax are given in the text printed by \ref
#   abstraction_printHelpAndExit().
#
def abstraction_parseArgv(
  argv  #!< vector of arguments as accessed whith <tt> sys.argv </tt>
):
  global abstraction_concreteModelFilePath
  global abstraction_abstractModelFilePathList
  global abstraction_verboseMode

  if ( len(argv) < 2 ):
    print("** Error ** you must specify at least one argument")
    exit(1)

  previousArg = str()
  for theArg in argv[1:len(argv)]:
    if ( str.find(theArg, "-") == 0 ):
      if   ( str(theArg) == "-c" or str(theArg) == "--concrete" ):
        previousArg = "concrete"
      elif ( str(theArg) == "-a" or str(theArg) == "--abstract" ):
        previousArg = "abstract"
      elif ( str(theArg) == "-v" or str(theArg) == "--verbose"  ):
        abstraction_verboseMode = True
      elif ( str(theArg) == "-h" or str(theArg) == "--help"     ):
        abstraction_printHelpAndExit(0)
      else:
        print("** Error ** unable to parse argument: " + str(theArg))
        exit(2)
    else:
      if   ( str(previousArg) == "concrete" ):
        abstraction_concreteModelFilePath = str(theArg)
        previousArg = ""
      elif ( str(previousArg) == "abstract" ):
        list.append(abstraction_abstractModelFilePathList, str(theArg))
      else:
        print("** Error ** unable to parse argument: " \
                + str(previousArg) + str(theArg) )

  fileExists = True
  if ( not os.path.exists(str(abstraction_concreteModelFilePath)) ):
    fileExists = False
  #
  for thePath in abstraction_abstractModelFilePathList:
    if ( not os.path.exists(str(thePath)) ):
      fileExists = False
      break

  if ( not fileExists ):
    print("** Error ** you must specify existing files for the concrete and " \
          "abstracted models.")
    exit(3)

#===============================================================================
#===============================================================================


abstraction_parseArgv(sys.argv)

# loads the concrete model and accesses its transitions:
pynusmv.init.init_nusmv()
theConcreteTransList = abstraction_getTransList(abstraction_concreteModelFilePath)

for theAbstractedModel in abstraction_abstractModelFilePathList:
  pynusmv.init.reset_nusmv()
  theAbstractTransList = abstraction_getTransList(theAbstractedModel)

  print("== verifying transitions equivalence between abstracted model " \
        + str(theAbstractedModel) + " and the concrete model.")

  isOk = True
  for theAbstractTrans in theAbstractTransList:
    theAbstractTransName = str.split(theAbstractTrans, " ")
    theAbstractTransName = theAbstractTransName[0]

    if ( theAbstractTrans not in theConcreteTransList ):
      print("** Error ** transition function " + str(theAbstractTransName) \
        + " is not in the concrete model.")
      isOk = False

      # debug informations:
      if ( abstraction_verboseMode == True ):
        theConcreteTransWithSameName = ""
        for theConcreteTrans in theConcreteTransList:
          theConcreteTransName = str.split(theConcreteTrans, " ")
          theConcreteTransName = theConcreteTransName[0]
          if ( theAbstractTransName in theConcreteTransName or \
               theConcreteTransName in theAbstractTransName ):
            theConcreteTransWithSameName = theConcreteTrans
            break
        #
        if ( theConcreteTransWithSameName == "" ):
          print("No transition with the same name has been found in the" \
                " concrete model.")
        else:
          print("### In the abstracted model:")
          print(theAbstractTrans)
          print("")
          print("### In the concrete model:")
          print(theConcreteTransWithSameName)
          print("")

  if ( not isOk ):
    print("The concrete model is not a refinement of the abstracted model.")
    print("")
    exit(1)
  else:
    print("The concrete model is a refinement of the abstracted model.")
    print("")

