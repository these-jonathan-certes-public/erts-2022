/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

module packets_decompresser
#(
  parameter p_none      = 4'b0000,  //!< when no packet is decompressed
  parameter p_isync     = 4'b0001,  //!< to encode packet identifier isync
  parameter p_atom      = 4'b0010,  //!< to encode packet identifier atom
  parameter p_branch    = 4'b0011,  //!< to encode packet identifier branch
  parameter p_waypoint  = 4'b0100,  //!< to encode packet identifier waypoint
  parameter p_trigger   = 4'b0101,  //!< to encode packet identifier trigger
  parameter p_contextid = 4'b0110,  //!< to encode packet identifier contextid
  parameter p_vmid      = 4'b0111,  //!< to encode packet identifier vmid
  parameter p_timestamp = 4'b1000,  //!< to encode packet identifier timestamp
  parameter p_except    = 4'b1001,  //!< to encode packet identifier except
  parameter p_ignore    = 4'b1010   //!< to encode packet identifier ignore
) (
  input       reset,      //!< reset
  input       clk,        //!< clock
  input [7:0] in,         //!< data input
  input       enable,     //!< enable bit
  //
  output [3:0] available_packet,  //!< indicates which packet is currently
                                  //!  decompressing
  output ready,    //!< packet decompression has terminated
  //
  output [3:0] data_size, //!< number of useful bytes in data
  output [7:0] data__0,   //!< one byte of output data, index  0
  output [7:0] data__1,   //!< one byte of output data, index  1
  output [7:0] data__2,   //!< one byte of output data, index  2
  output [7:0] data__3,   //!< one byte of output data, index  3
  output [7:0] data__4,   //!< one byte of output data, index  4
  output [7:0] data__5,   //!< one byte of output data, index  5
  output [7:0] data__6,   //!< one byte of output data, index  6
  output [7:0] data__7,   //!< one byte of output data, index  7
  output [7:0] data__8,   //!< one byte of output data, index  8
  output [7:0] data__9,   //!< one byte of output data, index  9
  output [7:0] data_10,   //!< one byte of output data, index 10
  output [7:0] data_11,   //!< one byte of output data, index 11
  output [7:0] data_12,   //!< one byte of output data, index 12
  output [7:0] data_13,   //!< one byte of output data, index 13
  output [7:0] data_14    //!< one byte of output data, index 14
);

  localparam state_data__0 = 4'b0000;
  localparam state_data__1 = 4'b0001;
  localparam state_data__2 = 4'b0010;
  localparam state_data__3 = 4'b0011;
  localparam state_data__4 = 4'b0100;
  localparam state_data__5 = 4'b0101;
  localparam state_data__6 = 4'b0110;
  localparam state_data__7 = 4'b0111;
  localparam state_data__8 = 4'b1000;
  localparam state_data__9 = 4'b1001;
  localparam state_data_10 = 4'b1010;
  localparam state_data_11 = 4'b1011;
  localparam state_data_12 = 4'b1100;
  localparam state_data_13 = 4'b1101;
  localparam state_data_14 = 4'b1110;
  //
  reg [3:0] state;

  reg [3:0] r_packet;

  reg [7:0] r_data__0;
  reg [7:0] r_data__1;
  reg [7:0] r_data__2;
  reg [7:0] r_data__3;
  reg [7:0] r_data__4;
  reg [7:0] r_data__5;
  reg [7:0] r_data__6;
  reg [7:0] r_data__7;
  reg [7:0] r_data__8;
  reg [7:0] r_data__9;
  reg [7:0] r_data_10;
  reg [7:0] r_data_11;
  reg [7:0] r_data_12;
  reg [7:0] r_data_13;
  reg [7:0] r_data_14;
  //
  reg [3:0] r_data_size;

  initial begin
    state       = state_data__0;
    r_data_size = 4'b0000;
    //
    r_data__0 = 8'b00000000;
    r_data__1 = 8'b00000000;
    r_data__2 = 8'b00000000;
    r_data__3 = 8'b00000000;
    r_data__4 = 8'b00000000;
    r_data__5 = 8'b00000000;
    r_data__6 = 8'b00000000;
    r_data__7 = 8'b00000000;
    r_data__8 = 8'b00000000;
    r_data__9 = 8'b00000000;
    r_data_10 = 8'b00000000;
    r_data_11 = 8'b00000000;
    r_data_12 = 8'b00000000;
    r_data_13 = 8'b00000000;
    r_data_14 = 8'b00000000;
  end

  /****************************************************************************/

  always @( posedge(clk) ) begin
    if ( reset != 1'b0) begin
      state       <= state_data__0;
      r_data_size <= 4'b0000;
      //
      r_data__0 <= 8'b00000000;
      r_data__1 <= 8'b00000000;
      r_data__2 <= 8'b00000000;
      r_data__3 <= 8'b00000000;
      r_data__4 <= 8'b00000000;
      r_data__5 <= 8'b00000000;
      r_data__6 <= 8'b00000000;
      r_data__7 <= 8'b00000000;
      r_data__8 <= 8'b00000000;
      r_data__9 <= 8'b00000000;
      r_data_10 <= 8'b00000000;
      r_data_11 <= 8'b00000000;
      r_data_12 <= 8'b00000000;
      r_data_13 <= 8'b00000000;
      r_data_14 <= 8'b00000000;
    end
    else if (enable == 1'b1) begin
      case ( available_packet )
        p_isync : begin
          case (state)
            state_data__0 : begin
              r_data_size <= 1;
              state    <= state_data__1;
              r_packet <= p_isync;
              //
              r_data__0 <= in;
            end

            state_data__1 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data__2;
              //
              r_data__1 <= in;
            end

            state_data__2 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data__3;
              //
              r_data__2 <= in;
            end

            state_data__3 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data__4;
              //
              r_data__3 <= in;
            end

            state_data__4 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data__5;
              //
              r_data__4 <= in;
            end

            state_data__5 : begin
              if ( in[6:5] != 2'b00 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__6;
              end
              else if ( in[6:5] == 2'b00 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_11;
              end
              //
              r_data__5 <= in;
            end

            state_data__6 : begin
              if ( in[6] != 1'b0 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__7;
              end
              else if ( in[6] == 1'b0 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_11;
              end
              //
              r_data__6 <= in;
            end

            state_data_11 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data_12;
              //
              r_data_11 <= in;
            end

            state_data__7 : begin
              if ( in[7] != 1'b0 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__8;
              end
              else if ( in[7] == 1'b0 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_11;
              end
              //
              r_data__7 <= in;
            end

            state_data__8 : begin
              if ( in[7] != 1'b0 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__9;
              end
              else if ( in[7] == 1'b0 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_11;
              end
              //
              r_data__8 <= in;
            end

            state_data__9 : begin
              if ( in[7] != 1'b0 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              else if ( in[7] == 1'b0 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_11;
              end
              //
              r_data__9 <= in;
            end

            state_data_10 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data_11;
              //
              r_data_10 <= in;
            end

            state_data_12 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data_13;
              //
              r_data_12 <= in;
            end

            state_data_13 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data_14;
              //
              r_data_13 <= in;
            end

            state_data_14 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data_14 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // isync

        p_atom : begin
          case (state)
            state_data__0 : begin
              if ( in[6] == 1'b1 ) begin
                r_data_size <= 1;
                state    <= state_data__1;
                r_packet <= p_atom;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__0 <= in;
            end

            state_data__1 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__2;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__1 <= in;
            end

            state_data__2 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__3;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__2 <= in;
            end

            state_data__3 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__4;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__3 <= in;
            end

            state_data__4 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data__4 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // atom

        p_branch : begin
          case (state)
            state_data__0 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= 1;
                state    <= state_data__1;
                r_packet <= p_branch;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= 1;
                state    <= state_data__7;
                r_packet <= p_branch;
              end
              //
              r_data__0 <= in;
            end

            state_data__1 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__2;
              end
              else if ( in[7:6] == 2'b01 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__5;
              end
              else if ( in[7:6] == 2'b00 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__7;
              end
              //
              r_data__1 <= in;
            end

            state_data__7 : begin
              if ( in[6] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__8;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__7 <= in;
            end

            state_data__2 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__3;
              end
              else if ( in[7:6] == 2'b01 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__5;
              end
              else if ( in[7:6] == 2'b00 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__7;
              end
              //
              r_data__2 <= in;
            end

            state_data__5 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__6;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__7;
              end
              //
              r_data__5 <= in;
            end

            state_data__3 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__4;
              end
              else if ( in[7:6] == 2'b01 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__5;
              end
              else if ( in[7:6] == 2'b00 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__7;
              end
              //
              r_data__3 <= in;
            end

            state_data__4 : begin
              if ( in[6] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__5;
              end
              else if ( in[6] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__7;
              end
              //
              r_data__4 <= in;
            end

            state_data__6 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data__7;
              //
              r_data__6 <= in;
            end

            state_data__8 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__9;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__8 <= in;
            end

            state_data__9 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__9 <= in;
            end

            state_data_10 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_11;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data_10 <= in;
            end

            state_data_11 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data_11 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // branch

        p_waypoint : begin
          case (state)
            state_data__0 : begin
              r_data_size <= 1;
              state    <= state_data__1;
              r_packet <= p_waypoint;
              //
              r_data__0 <= in;
            end

            state_data__1 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__2;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__1 <= in;
            end

            state_data__2 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__3;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__2 <= in;
            end

            state_data__3 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__4;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__3 <= in;
            end

            state_data__4 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__5;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__4 <= in;
            end

            state_data__5 : begin
              if ( in[6] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__6;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data__5 <= in;
            end

            state_data__6 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data__6 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // waypoint

        p_trigger : begin
          case (state)
            state_data__0 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data__0 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // trigger

        p_contextid : begin
          case (state)
            state_data__0 : begin
              r_data_size <= 1;
              state    <= state_data__1;
              r_packet <= p_contextid;
              //
              r_data__0 <= in;
            end

            state_data__1 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data__2;
              //
              r_data__1 <= in;
            end

            state_data__2 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data__3;
              //
              r_data__2 <= in;
            end

            state_data__3 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data__4;
              //
              r_data__3 <= in;
            end

            state_data__4 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data__4 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // contextid

        p_vmid : begin
          case (state)
            state_data__0 : begin
              r_data_size <= 1;
              state    <= state_data__1;
              r_packet <= p_vmid;
              //
              r_data__0 <= in;
            end

            state_data__1 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data__1 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // vmid

        p_timestamp : begin
          case (state)
            state_data__0 : begin
              r_data_size <= 1;
              state    <= state_data__1;
              r_packet <= p_timestamp;
              //
              r_data__0 <= in;
            end

            state_data__1 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__2;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              //
              r_data__1 <= in;
            end

            state_data__2 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__3;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              //
              r_data__2 <= in;
            end

            state_data_10 : begin
              if ( in[6] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_11;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data_10 <= in;
            end

            state_data__3 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__4;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              //
              r_data__3 <= in;
            end

            state_data__4 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__5;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              //
              r_data__4 <= in;
            end

            state_data__5 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__6;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              //
              r_data__5 <= in;
            end

            state_data__6 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__7;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              //
              r_data__6 <= in;
            end

            state_data__7 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__8;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              //
              r_data__7 <= in;
            end

            state_data__8 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data__9;
              end
              else if ( in[7] != 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_10;
              end
              //
              r_data__8 <= in;
            end

            state_data__9 : begin
              r_data_size <= r_data_size + 1;
              state <= state_data_10;
              //
              r_data__9 <= in;
            end

            state_data_11 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_12;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data_11 <= in;
            end

            state_data_12 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_13;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data_12 <= in;
            end

            state_data_13 : begin
              if ( in[7] == 1'b1 ) begin
                r_data_size <= r_data_size + 1;
                state <= state_data_14;
              end
              else begin
                r_data_size <= 0;
                // here it must be ready
                state    <= state_data__0;
                r_packet <= p_none;
              end
              //
              r_data_13 <= in;
            end

            state_data_14 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data_14 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // timestamp

        p_except : begin
          case (state)
            state_data__0 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data__0 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // except

        p_ignore : begin
          case (state)
            state_data__0 : begin
              r_data_size <= 0;
              // here it must be ready
              state    <= state_data__0;
              r_packet <= p_none;
              //
              r_data__0 <= in;
            end

            default : begin
              // handling errors
              state    <= state_data__0;
              r_packet <= p_none;
            end
          endcase
        end // ignore

        default : begin
          // handling errors
          r_packet <= p_none;
        end
      endcase
    end
  end

  /****************************************************************************/

  assign ready = (reset != 1'b1) && (enable == 1'b1) && (
       (available_packet == p_isync) && (state == state_data_14)
    || (available_packet == p_atom) && (state == state_data__0) && (in[6] != 1'b1)
    || (available_packet == p_atom) && (state == state_data__1) && (in[7] != 1'b1)
    || (available_packet == p_atom) && (state == state_data__2) && (in[7] != 1'b1)
    || (available_packet == p_atom) && (state == state_data__3) && (in[7] != 1'b1)
    || (available_packet == p_atom) && (state == state_data__4)
    || (available_packet == p_branch) && (state == state_data__7) && (in[6] != 1'b1)
    || (available_packet == p_branch) && (state == state_data__8) && (in[7] != 1'b1)
    || (available_packet == p_branch) && (state == state_data__9) && (in[7] != 1'b1)
    || (available_packet == p_branch) && (state == state_data_10) && (in[7] != 1'b1)
    || (available_packet == p_branch) && (state == state_data_11)
    || (available_packet == p_waypoint) && (state == state_data__1) && (in[7] != 1'b1)
    || (available_packet == p_waypoint) && (state == state_data__2) && (in[7] != 1'b1)
    || (available_packet == p_waypoint) && (state == state_data__3) && (in[7] != 1'b1)
    || (available_packet == p_waypoint) && (state == state_data__4) && (in[7] != 1'b1)
    || (available_packet == p_waypoint) && (state == state_data__5) && (in[6] != 1'b1)
    || (available_packet == p_waypoint) && (state == state_data__6)
    || (available_packet == p_trigger) && (state == state_data__0)
    || (available_packet == p_contextid) && (state == state_data__4)
    || (available_packet == p_vmid) && (state == state_data__1)
    || (available_packet == p_timestamp) && (state == state_data_10) && (in[6] != 1'b1)
    || (available_packet == p_timestamp) && (state == state_data_11) && (in[7] != 1'b1)
    || (available_packet == p_timestamp) && (state == state_data_12) && (in[7] != 1'b1)
    || (available_packet == p_timestamp) && (state == state_data_13) && (in[7] != 1'b1)
    || (available_packet == p_timestamp) && (state == state_data_14)
    || (available_packet == p_except) && (state == state_data__0)
    || (available_packet == p_ignore) && (state == state_data__0)
  );

  /*
   * outputs are only correct when ready:
   */
  assign data_size = !(ready) ? 4'b0000 : (r_data_size + 1);
  //
  assign data__0 = !(ready) ? 8'b00000000 : (state == state_data__0) ? in : r_data__0;
  assign data__1 = !(ready) ? 8'b00000000 : (state == state_data__1) ? in : r_data__1;
  assign data__2 = !(ready) ? 8'b00000000 : (state == state_data__2) ? in : r_data__2;
  assign data__3 = !(ready) ? 8'b00000000 : (state == state_data__3) ? in : r_data__3;
  assign data__4 = !(ready) ? 8'b00000000 : (state == state_data__4) ? in : r_data__4;
  assign data__5 = !(ready) ? 8'b00000000 : (state == state_data__5) ? in : r_data__5;
  assign data__6 = !(ready) ? 8'b00000000 : (state == state_data__6) ? in : r_data__6;
  assign data__7 = !(ready) ? 8'b00000000 : (state == state_data__7) ? in : r_data__7;
  assign data__8 = !(ready) ? 8'b00000000 : (state == state_data__8) ? in : r_data__8;
  assign data__9 = !(ready) ? 8'b00000000 : (state == state_data__9) ? in : r_data__9;
  assign data_10 = !(ready) ? 8'b00000000 : (state == state_data_10) ? in : r_data_10;
  assign data_11 = !(ready) ? 8'b00000000 : (state == state_data_11) ? in : r_data_11;
  assign data_12 = !(ready) ? 8'b00000000 : (state == state_data_12) ? in : r_data_12;
  assign data_13 = !(ready) ? 8'b00000000 : (state == state_data_13) ? in : r_data_13;
  assign data_14 = !(ready) ? 8'b00000000 : (state == state_data_14) ? in : r_data_14;

  assign available_packet = (reset != 1'b0)          ? p_none      :
                            (state != state_data__0) ? r_packet    :
                            (enable != 1'b1)         ? p_none      :
    (in == 8'b00001000)                              ? p_isync     :
    (in[7] == 1'b1) && (in[0] == 1'b0)               ? p_atom      :
    (in[0] == 1'b1)                                  ? p_branch    :
    (in == 8'b01110010)                              ? p_waypoint  :
    (in == 8'b00001100)                              ? p_trigger   :
    (in == 8'b01101110)                              ? p_contextid :
    (in == 8'b00111100)                              ? p_vmid      :
    ((in & 8'b11111011) == 8'b01000010)              ? p_timestamp :
    (in == 8'b01110110)                              ? p_except    :
    (in == 8'b01100110)                              ? p_ignore    : p_none;

endmodule
